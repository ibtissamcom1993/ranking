import json 
from collections import defaultdict
import nltk 
from nltk.corpus import stopwords
from rank_bm25 import BM25Okapi
from langdetect import detect

nltk.download('stopwords')

class RankingSystem:
    def __init__(self, documents_file, index_file):
        """
        Initialize the RankingSystem with documents and index.

        Parameters
        ----------
        documents_file : str
            File path to the documents JSON file.

        index_file : str
            File path to the title index JSON file.
        """
        self.documents = self.load_documents(documents_file)
        self.index = self.load_index(index_file)
        print("Documents and index loaded successfully.")

    def load_documents(self, documents_file):
        """
        Load documents from the JSON file.

        Parameters
        ----------
        documents_file : str
            File path to the documents JSON file.

        Returns
        -------
        documents : dict
            Dictionary containing document data.
        """
        with open(documents_file, "r", encoding="utf-8") as file:
            documents = json.load(file)
        print("Documents loaded successfully.")
        return documents
    
    def load_index(self, index_file):
        """
        Load title index from the JSON file.

        Parameters
        ----------
        index_file : str
            File path to the title index JSON file.

        Returns
        -------
        index : dict
            Dictionary containing the title index.
        """
        with open(index_file, "r", encoding="utf-8") as file:
            index = json.load(file)
        print("Index loaded successfully.")
        return index

    def filter_documents(self, query, all_tokens=True):
        """
        Filter documents based on the tokens present in the user query.

        Parameters
        ----------
        query : str
            User query to filter documents.

        all_tokens : bool, optional
            If True, filters documents containing all query tokens.
            If False, filters documents containing at least one query token.

        Returns
        -------
        filtered_documents : dict
            Filtered documents containing only those documents that match the specified condition.
        """
        print("Filtering documents...")
        # Tokenize the user query
        query_tokens = nltk.word_tokenize(query.lower())
        filtered_documents = {}  # Initialize with default values
        
        if all_tokens:
            # Initialize a set to store document IDs containing all query tokens
            documents_containing_all_tokens = set()
    
            # Find documents containing all tokens in the query
            for token in query_tokens:
                if token in self.index:
                    # If the set is empty, initialize it with the document IDs for the current token
                    if not documents_containing_all_tokens:
                        documents_containing_all_tokens = set(self.index[token].keys())
                    else:
                        # Take the intersection of document IDs containing current token and previously found documents
                        documents_containing_all_tokens &= set(self.index[token].keys())
    
            # Build the filtered documents dictionary
            for doc_id in documents_containing_all_tokens:
                doc_contains_all_tokens = all(token in self.index.keys() for token in query_tokens)
                if doc_contains_all_tokens:
                    doc_details = {token: self.index[token][doc_id] for token in query_tokens if doc_id in self.index[token]}
                    filtered_documents[doc_id] = {
                        "positions": [doc_details[token]['positions'] for token in query_tokens if token in doc_details],
                        "count": sum(doc_details[token]['count'] for token in query_tokens if token in doc_details)
                    }
        else:
            # Find documents containing at least one token in the query
            for token in query_tokens:
                if token in self.index:
                    for doc_id, details in self.index[token].items():
                        if doc_id not in filtered_documents:
                            filtered_documents[doc_id] = {"positions": details.get("positions"), "count": details["count"]}
                        else:
                            filtered_documents[doc_id]["positions"].extend(details.get("positions"))
                            filtered_documents[doc_id]["count"] += details["count"]

        #print("Filtered documents:", filtered_documents)  # to debug
        return filtered_documents

    def calculate_token_weight(self, token, stop_words):
        """
        Calculate the weight of a token based on its relevance relative to stop words.

        Parameters
        ----------
        token : str
            Token to calculate the weight for.

        stop_words : set
            Set of stop words for the document language.

        Returns
        -------
        weight : int
            Weight of the token.
        """
        if token not in stop_words:
            return 2  # Apply a higher weight to meaningful tokens
        else:
            return 1  # Apply a lower weight to stop words
    
    def calculate_ranking_score(self, document_info):
        """
        Calculate the ranking score for a document based on its tokens and weights.

        Parameters
        ----------
        document_info : dict
            Dictionary containing information about the document, such as positions and counts.

        Returns
        -------
        score : float
            Ranking score for the document.
        """
        # Language detection of the document
        titles_concatenated = ' '.join([document['title'] for document in self.documents])
        doc_language = detect(titles_concatenated)

        # Setting stopwords based on the language of the document
        if doc_language == 'en':
            stop_words = set(stopwords.words('english'))
        elif doc_language == 'de':
            stop_words = set(stopwords.words('german'))
        elif doc_language == 'es':
            stop_words = set(stopwords.words('spanish'))
        else:
            # If the document language is not English, German, or Spanish, consider it as French
            stop_words = set(stopwords.words('french'))

        # Extract the text from documents
        texts = [document['title'] for document in self.documents]

        # Tokenize the text
        tokenized_texts = [nltk.word_tokenize(text.lower()) for text in texts]

        # Preprocessing title tokens by removing stopwords and calculating weights
        weighted_tokens = [self.calculate_token_weight(token, stop_words) for tokens in tokenized_texts for token in tokens]

        # Calculating the weighted sum of tokens
        ranking_score = sum(weighted_tokens)

        return ranking_score


    def linear_ranking(self, filtered_documents):
        """
        Implement a linear ranking function to order the filtered documents.

        Parameters
        ----------
        filtered_documents : dict
            Filtered documents containing relevant information such as positions and counts.

        Returns
        -------
        ranked_documents : list
            List of documents ordered based on the linear ranking function.
        """
        print("Performing linear ranking...")
        ranked_documents = sorted(filtered_documents.items(), key=lambda x: (self.calculate_ranking_score(x[1]), x[1]['count'], min(x[1]['positions'])) if x[1]['positions'] else (self.calculate_ranking_score(x[1]), x[1]['count'], float('inf')), reverse=True)
        return ranked_documents

    def calculate_bm25_score(self, document_tokens, query):
        """
        Calculate the BM25 score for a document based on its tokens and the user query.

        Parameters
        ----------
        document_tokens : dict
            Dictionary containing tokens of the document (title and content).

        query : str
            User query.

        Returns
        -------
        score : float
            BM25 score for the document.
        """
        # Language detection of the document
        titles_concatenated = ' '.join([document['title'] for document in self.documents])
        doc_language = detect(titles_concatenated)      

        # Setting stopwords based on the language of the document
        if doc_language == 'en':
            stop_words = set(stopwords.words('english'))
        elif doc_language == 'de':
            stop_words = set(stopwords.words('german'))
        elif doc_language == 'es':
            stop_words = set(stopwords.words('spanish'))
        else:
            # If the document language is not English, German, or Spanish, consider it as French
            stop_words = set(stopwords.words('french'))

        # Extract the text from documents
        texts = [document['title'] for document in self.documents]

        # Tokenize the text
        tokenized_texts = [nltk.word_tokenize(text.lower()) for text in texts]

        # Preprocessing title tokens by removing stopwords
        title_tokens = [token for tokens in tokenized_texts for token in tokens if token not in stop_words]

        # Calculating BM25 score for the title
        bm25_title = BM25Okapi([title_tokens])
        title_score = bm25_title.get_scores(query)

        return title_score[0]
    
    def retrieve_document_details(self, ranked_documents):
        """
        Retrieve document titles and URLs based on ranked document IDs.

        Parameters
        ----------
        ranked_documents : list
            List of ranked documents containing document IDs.

        Returns
        -------
        result_documents : list
            List of documents with titles and URLs.
        """
        print("Retrieving document details...")
        result_documents = []
        for doc_id_str, _ in ranked_documents:
            doc_id = int(doc_id_str)  
            for document in self.documents:
                if document['id'] == doc_id:
                    title = document.get('title')
                    url = document.get('url')
                    result_documents.append({"title": title, "url": url})
                    break  
        print("Document details retrieved successfully.")
        #print("Retrieved document details:", result_documents)  # to debug
        return result_documents

    
    def execute_query(self, query_tokens, all_tokens):
        """
        Execute a query to filter, rank, and retrieve relevant documents.

        Parameters
        ----------
        query_tokens : list
            List of tokens extracted from the user query.

        Returns
        -------
        results : dict
            Dictionary containing total documents, filtered documents count, and list of relevant documents with titles and URLs.
        """
        print("Executing query...")
        # Filter documents based on query tokens
        filtered_documents = self.filter_documents(query_tokens,all_tokens)

        # Rank filtered documents
        ranked_documents = self.linear_ranking(filtered_documents)

        # Retrieve document details (titles and URLs)
        result_documents = self.retrieve_document_details(ranked_documents)

        # Construct results dictionary
        results = {
            "total_documents": len(self.index),
            "filtered_documents": len(result_documents),
            "documents": result_documents
        }
        return results

    
    def save_results(self, results_file, results):
        """
        Save the results to a JSON file.

        Parameters
        ----------
        results_file : str
            File path to save the results.

        results : dict
            Dictionary containing the results to be saved.
        """
        print("Saving results...")
        with open(results_file, "w") as file:
            json.dump(results, file, indent=2)
        print("Results saved successfully.")
