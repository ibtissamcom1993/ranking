import json
from RankingSystem import RankingSystem

def main():
    """
    Main function to execute the ranking system workflow.
    """
    # File paths
    documents_file = "documents.json"
    index_file = "title_pos_index.json"

    # Initialize the RankingSystem
    ranking_system = RankingSystem(documents_file, index_file)

    # User choice
    user_choice = input("Would you like to enter your own query? (y/n): ")

    if user_choice.lower() == "y":
        # Get user input for the query
        user_query = input("Enter your query: ")
        # Ask the user for filtering preference
        filter_option = input("Filter based on all tokens? (y/n): ").lower()
        all_tokens = True if filter_option == "y" else False
        # Execute the query with the specified filter option
        results = ranking_system.execute_query(user_query, all_tokens)
    else:
        # Use the default query
        default_query = "la ligue des champions " 
        # Ask the user for filtering preference
        filter_option = input("Filter based on all tokens? (y/n): ").lower()
        all_tokens = True if filter_option == "y" else False
        # Execute the default query
        results = ranking_system.execute_query(default_query, all_tokens)

    # Save the results to a JSON file
    results_file = "results.json"
    ranking_system.save_results(results_file, results)

if __name__ == "__main__":
    main()