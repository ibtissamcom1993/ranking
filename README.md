# Ranking System

The objective of this project is to implement a ranking system for retrieving relevant documents based on user requests. The system reads a user query, tokenizes it, filters the documents containing the query tokens, ranks the filtered documents based on various features such as token importance and position, and returns the top-ranked documents along with their titles and URLs.

## System Workflow

1. **User makes a request**: The user inputs their query into the system.

2. **Tokenize the request**: The query is tokenized into individual words or tokens.

3. **Filter documents based on the tokenized request**: The system filters the documents based on the presence of the query tokens.

4. **Rank the filtered documents based on a score**: The filtered documents are ranked based on a scoring mechanism that evaluates their relevance to the query.

5. **Get ranked documents with their information**: The top-ranked documents are retrieved along with their titles and URLs.

![System Workflow Diagram](diag_act.png)

## Getting Started

### Prerequisites

Make sure you have Python installed. You can install the required packages by running:

```bash
pip install -r requirements.txt
```


### Running the Main Workflow

1. Clone the repository to your local machine.
2. Make sure you have the required JSON files:
    - documents.json: Contains the document data.
    - title_pos_index.json: Contains the title index.
3. Run the main.py script using the following command:

```bash
python3 main.py
```

## Project Outputs

The project outputs the following:

- Filtered documents based on user query tokens.
- Ranked documents according to the linear ranking function.
- Retrieved document details (titles and URLs).
- Results saved to a JSON file.


## Project Structure
- `RankingSystem.py`: Contains the implementation of the ranking system.
- `main.py` : Entry point of the program where the main workflow is executed.
- `documents.json` : JSON file containing document data.
- `title_pos_index.json` : JSON file containing the title index.
- `README.md` : Documentation explaining the project setup, execution, and outputs.


## Input by the User
The user can interact with the system in the following ways:

- Entering their own query when prompted.
- Choosing whether to filter documents based on all query tokens or at least one query token.


## Author 

**LOUKILI IBTISSAM**  